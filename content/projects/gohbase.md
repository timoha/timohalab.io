---
title: gohbase
when: Maintaining, Arista Networks
tags: ["Go"]
date: 2017-01-01
thumbnail: /img/gohbase-thumbnail.png
---

[gohbase](https://github.com/tsuna/gohbase) is a pure Go client library for [Apache HBase](https://hbase.apache.org). It's fast, slim, and most importantly not Java.

Check out my HBaseCon 2017 talk explaining its workings.

{{< youtube oj6ID2K8MJU >}}
