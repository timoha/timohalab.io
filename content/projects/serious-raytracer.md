---
title: Serious Raytracer
date: 2014-10-31
when: Fall 2014, CS 184, UC Berkeley
tags: ["C++"]
thumbnail: /img/raytracer-badass-thumbnail.png
---

{{< figure src="/img/raytracer-lambo.png" title="Badassness" caption="Interpolated normals, refractions, reflections of depth 10, acceleration data structures, and 16x anti-aliasing. Renders in 30min for 1000x1000px image on MacBook Pro 2011" >}}

Project was done with Shiv Sundram for Graphics course (CS184) at UC Berkeley. The raytracer has the following features:

- Materials
- Reflections
- Refractions
- Point lights
- Direct lights
- Anti-aliasing
- Normals interpolation
- AABB-tree for rendering speed optimization
- Custom file format for scene setup
- .obj file rendering

Source: [https://github.com/shivsundram/raytracer](https://github.com/shivsundram/raytracer)

More renderings:

{{< figure src="/img/raytracer-refract.png" title="Refractions" caption="Square tiling material, refraction+reflection of sphere with coefficient of 1.33 with 16x anti-aliasing" >}}

{{< figure src="/img/raytracer-su39.png" title="Pink SU39 Fighter Jet Because Why Not?" caption="16x anti-aliasing" >}}
