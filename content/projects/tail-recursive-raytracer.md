---
title: Tail-recursive Raytracer
date: 2013-12-12
when: Fall 2013, CS 61A, UC Berkeley
tags: ["Scheme", "Python"]
thumbnail: /img/scheme-thumbnail.png
---

{{< figure src="/img/winning-entry.png" title="Winning Entry" >}}

Source: [https://github.com/timoha/rt](https://github.com/timoha/rt)

“Recursive Art Contest” winning entry that [Michael Schuldt](http://michael.schuldt.io/) and I implemented in Scheme for CS61A during Fall 2013. The Scheme is implementation is custom and was done in Python. Couple notable feature of our Scheme is that it supports multi-processing which allowed us to utilize multiple cores for faster render time as well as uses Turtle drawing library.

The scene is a sampled down [Apollonian gasket](https://en.wikipedia.org/wiki/Apollonian_gasket) fractal that reminds a drop of water sliding down a sphere.

More renderings done by the raytracer:

{{< figure src="/img/cs61a-first-render.png" title="First Rendering" >}}
{{< figure src="/img/cs61a-inverted.png" title="Dotted with Inverted Colors" >}}

Michael and I also were invited to give CS61A extra lecture about raytracers:

<iframe src="https://docs.google.com/presentation/d/1y-q5RwaMp2YHkmLCUrqP5kEs2y_0cBZS529ldNHu6hE/embed?start=false&loop=true&delayms=3000" frameborder="0" width="620" height="494" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
