---
title: Whiteboard
date: 2014-08-30
when: Summer 2014, Wix.com
tags: ["Haskell", "Elm", "PostgreSQL"]
thumbnail: /img/whiteboard-thumbnail.png
---

WiX.com widget application which allows visitors to leave drawings on a shared canavas in real-time. Developed during my internship at [WiX.com](https://wix.com). I had a great time using wonderful [Elm](https://elm-lang.org/) to develop the painter as well as Haskell to develop real-time back-end server.

The website owners can also download real-size PDF (select between inches/centimeters) of the board at any time as well as customize the look (borders, canvas color, etc).

Work on both mobile and desktop.

Source: [https://github.com/timoha/whiteboard-tpa](https://github.com/timoha/whiteboard-tpa)

{{< figure src="https://github.com/andreywix/whiteboard-tpa/raw/master/wireframes/animated/color-panel.gif" title="Color Panel" caption="Select color, opacity, and stroke width" >}}

{{< figure src="https://github.com/andreywix/whiteboard-tpa/raw/master/wireframes/map-panel.png" title="Map Panel" caption="Used to navigate the board by dragging around" >}}


