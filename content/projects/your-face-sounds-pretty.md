---
title: Your Face Sounds Pretty ;)
date: 2015-12-18
when: Fall 2016, Music 158A, UC Berkeley
tags: ["Max", "Javascript", "Node.js"]
thumbnail: /img/face-thumbnail.png
---

My final project submission for Music 158A. Using [Max](https://cycling74.com/products/max/) and a couple of Javascript libraries ([clmtrackr](https://github.com/auduno/clmtrackr) for tracking and [osc](https://github.com/automata/osc-web) for communicating with Max) I was able to make sounds with my face movements.

{{< vimeo 149474891 >}}
