---
title: HanaHaus Reservations
date: 2015-08-30
when: Summer 2015, SAP
tags: ["Django", "Angular", "PostgreSQL"]
thumbnail: /img/hanahaus-thumbnail.png
---

{{<figure src="/img/hanahaus.png" title="HanaHaus Front Page">}}

[HanaHaus Reservations](https://app.hanahaus.com/guest/reserve) is a web-application I was developing during my internship at SAP.

[HanaHaus](http://www.hanahaus.com/#home) is an open space located in Palo Alto, CA on University Ave. Visitors can book various types of rooms and seats hourly and pay via credit card either in-person using iPads or through the application in advance.

The project was interesting because we had to implement it in the most generic way (pricing, resources, locations, etc) since new open-spaces were planned to be opened at locations all over the world. The prices also have to be flexible with ability to add deals, coupon codes, holidays. 
Also, the analytics part (bounce rate, room utilization, revenue) was fascinating to implement since I got to play with all the data we collected using Presto engine.

