---
title: Résumé
type: resume
---

# Andrey Elenskiy
a@elenskiy.co, +1(650)215-8860, [elenskiy.co](/)

--------------------------------------------
## Experience

**[Arista Networks](https://www.arista.com/), Software Engineer** _(Jan 2016 - Present)_

- Implemented a distributed application management system for Arista's CloudVision on-premises solution which provided zero-touch experience and high availability for customers
- Lead development of distributed streaming telemetry platform that handles > 250k messages/s and is the core of Arista CloudVision
- Developed and maintained [gohbase](/projects/gohbase/), the most popular pure Go HBase client library
- Wrote a migration job that converted over a trillion of datapoints across hundreds of clusters in < 8 hours
- Refactored entire codebase and client applications of CloudVision to use a different encoding format developed on top of MsgPack to save 20% of storage costs
- Lead various efforts to reduce storage utilization totaling ~50% reduction in costs
- Technical point of contact within the company for anything related to HDFS, HBase, and Kafka
- Go to person for resolving the most obscure bugs and issues in a timely fashion and under high pressure due to deep understanding of the entire stack
- Gave a lot of advices on architecture of scalable distributed systems, performance optimization, robust and flexible code structure
- Mentored new hires and interns working on challenging project with successful on-time deliveries
- Regularly worked with customers to resolve any inquiries with high customer satisfaction and fast resolution times
- Conducted first round of interviews with high full-time conversion rate

**[SAP](https://www.sap.com/), Software Intern** _(Summer 2015)_

- Developed an online [reservation platform](/projects/hanahaus/) for an open workspace "HanaHaus"
- Implemented back-end, front-end and data analytics

**[ProTerra Lighting](http://proterraled.com), Freelance Software Engineer** _(Summer 2015)_

- Developed a prototype for two-factor authentication using biometric devices
- Worked with vein map and fingerprint device SDKs

**[WiX.com](https://www.wix.com), Software Intern** _(Summer 2014)_

- Developed a [collaborative real-time whiteboard](/projects/whiteboard/) for Wix App market
- Developed a web server for file uploading application for Wix App market

## Technical Skills

**Talks**
:   Presented gohbase, a pure Go HBase client library, at HBaseCon2017 at Google HQ

**Programming Languages**
:   Go, Java, Python, C, Javascript, C++, Haskell, Elm, Scheme, PHP, HTML/CSS

**Databases/Distributed systems**
:   Hadoop, HBase, Kafka, Zookeeper, etcd, Prometheus, PostgreSQL, MongoDB

**Frameworks**
:   Django, Node.js, Qt, Angular, React

**DevOps**
:   Docker, Kubernetes, Ansible, GCP, Linux (CoreOS, NixOS, Arch, Ubuntu), replacing hard drives

## Education

**University of California, Berkeley** _(Sep 2013 - Dec 2015)_
Computer Science, BA, 3.66 GPA

**Foothill College** _(Sep 2010 - Jul 2013)_
Computer Science, AS

